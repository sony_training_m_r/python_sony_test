"""
Module to generate time table.
Based on subject file (subject .json).
Based on teacher filer (teacher.json).
We are usign recursive approch to assign teachers and subject to respective sections.
"""

import json

def generate():
    # generate the structure of timetable.json using teacher.json, subject.json 
    subject_data_f=open('subject.json', "r")
    teacher_data_f=open('teacher.json', "r")
    timetable_data_f=open('timetable.json', "r+")
    subject_data= json.loads(subject_data_f.read())
    teacher_data= json.loads(teacher_data_f.read())

    data = subject_data['data']
    teach_data=teacher_data['data']
    print(len(data),len(teach_data))
    if(len(data)==5 and len(teach_data)==11):
        no_of_teacher=11
        result_data={"data":[{"Section 1":[]},{"Section 2":[]},{"Section 3":[]},{"Section 4":[]},{"Section 5":[]}]}
    
   
        first_period="08:00AM-09:00AM"
        second_period="09:00AM-10:00AM"
        third_period="11:00AM-12:00PM"
        fourth_period="12:00PM-01:00PM"
    
        days=['MON','TUE','WED','THU','FRI']
        periods=[first_period,second_period,third_period,fourth_period]
        #["mon":{"time":[{"sub":subjectid}]}]
    
        #section 1
        temp_val=[]
        displacement_variable=0
        displacement_variable2=0
        # ABC logic for subject assignment
        displacement_variable2_template=[]
        temp_variable=displacement_variable2

        for i in range(5):
            displacement_variable2_template.append(temp_variable)
            temp_variable+=1
            if(temp_variable>no_of_teacher-1):
                temp_variable=0

        iter_variable=0

        for i in days:
            temp_val2={}
            for j in periods:
                temp_val2[j]=[data[displacement_variable],teach_data[displacement_variable2_template[iter_variable]]]
                displacement_variable+=1
                if(displacement_variable>4):
                    displacement_variable=0
                new_val=iter_variable+1
                if(new_val>4):
                    iter_variable=0
                else:
                    iter_variable=new_val
            displacement_variable2+=1
            if(displacement_variable2>10):
                displacement_variable2=0
            temp_val.append({i:[temp_val2]})
        result_data["data"][0]['Section 1']=temp_val


      
        #section 2
        temp_val=[]
        displacement_variable=1
        # ABC logic for subject assignment
        print(displacement_variable2)
        displacement_variable2_template=[]
        temp_variable=displacement_variable2

        for i in range(5):
            displacement_variable2_template.append(temp_variable)
            temp_variable+=1
            if(temp_variable>no_of_teacher-1):
                temp_variable=0

        iter_variable=0


        for i in days:
            temp_val2={}
            for j in periods:
                temp_val2[j]=[data[displacement_variable],teach_data[displacement_variable2_template[iter_variable]]]
                displacement_variable+=1
            
                if(displacement_variable>4):
                    displacement_variable=0
                new_val=iter_variable+1
                if(new_val>4):
                    iter_variable=0
                else:
                    iter_variable=new_val
            displacement_variable2+=1
            if(displacement_variable2>10):
                displacement_variable2=0
            temp_val.append({i:[temp_val2]})
        result_data["data"][1]['Section 2']=temp_val

     
        #section 3
        temp_val=[]
        displacement_variable=2
        #displacement_variable2=2
        displacement_variable2_template=[]
        temp_variable=displacement_variable2

        for i in range(5):
            displacement_variable2_template.append(temp_variable)
            temp_variable+=1
            if(temp_variable>no_of_teacher-1):
                temp_variable=0

        iter_variable=0
        # ABC logic for subject assignment
        for i in days:
            temp_val2={}
            for j in periods:
                temp_val2[j]=[data[displacement_variable],teach_data[displacement_variable2_template[iter_variable]]]
                displacement_variable+=1
                if(displacement_variable>4):
                    displacement_variable=0
                new_val=iter_variable+1
                if(new_val>4):
                    iter_variable=0
                else:
                    iter_variable=new_val
            displacement_variable2+=1
            if(displacement_variable2>10):
                displacement_variable2=0
            temp_val.append({i:[temp_val2]})
        result_data["data"][2]['Section 3']=temp_val

        #section 4
        temp_val=[]
        displacement_variable=3
        displacement_variable2_template=[]
        temp_variable=displacement_variable2

        for i in range(5):
            displacement_variable2_template.append(temp_variable)
            temp_variable+=1
            if(temp_variable>no_of_teacher-1):
                temp_variable=0

        iter_variable=0
        # ABC logic for subject assignment
        for i in days:
            temp_val2={}
            for j in periods:
                temp_val2[j]=[data[displacement_variable],teach_data[displacement_variable2_template[iter_variable]]]
                displacement_variable+=1
                if(displacement_variable>4):
                    displacement_variable=0
                new_val=iter_variable+1
                if(new_val>4):
                    iter_variable=0
                else:
                    iter_variable=new_val
            displacement_variable2+=1
            if(displacement_variable2>10):
                displacement_variable2=0
            temp_val.append({i:[temp_val2]})
        result_data["data"][3]['Section 4']=temp_val

        #section 5
        temp_val=[]
        displacement_variable=4
        displacement_variable2_template=[]
        temp_variable=displacement_variable2

        for i in range(5):
            displacement_variable2_template.append(temp_variable)
            temp_variable+=1
            if(temp_variable>no_of_teacher-1):
                temp_variable=0

        iter_variable=0
        #displacement_variable2=4
        # ABC logic for subject assignment
        for i in days:
            temp_val2={}
            for j in periods:
                temp_val2[j]=[data[displacement_variable],teach_data[displacement_variable2_template[iter_variable]]]
                displacement_variable+=1
                if(displacement_variable>4):
                    displacement_variable=0
                new_val=iter_variable+1
                if(new_val>4):
                    iter_variable=0
                else:
                    iter_variable=new_val
            displacement_variable2+=1
            if(displacement_variable2>10):
                displacement_variable2=0
            temp_val.append({i:[temp_val2]})
        result_data["data"][4]['Section 5']=temp_val

        timetable_data_f.seek(0)
        json.dump(result_data,timetable_data_f)
        print("Done..!\n")
        timetable_data_f.close()
        subject_data_f.close()
        teacher_data_f.close()

    else:
        print("Insufficient Data.")

def section_generate(section_number):
    # Display the section wise time table
    try:
        timetable_data_f=open('timetable.json', "r+")
        timetable_data= json.loads(timetable_data_f.read())
        timetable=timetable_data['data']

        temp_Val=["Section 1","Section 2","Section 3","Section 4","Section 5"]
        days=['MON','TUE','WED','THU','FRI']
        first_period="08:00AM-09:00AM"
        second_period="09:00AM-10:00AM"
        third_period="11:00AM-12:00PM"
        fourth_period="12:00PM-01:00PM"

        periods=[first_period,second_period,third_period,fourth_period]
        temp_data=timetable[section_number-1][temp_Val[section_number-1]]

        print("\nTIME TABLE OF SECTION ",str(section_number))
        print()
        print_var=[["Day",first_period,second_period,third_period,fourth_period]]
        for i in range(5):
            disp_temp=[days[i]]
            temp=0
            for j in periods:
                disp_temp.append(list(temp_data[i][days[i]][temp][j][0].values())[0]+"("+list(temp_data[i][days[i]][temp][j][1].values())[0]+")")
            print_var.append(disp_temp)
    
        for row in print_var:
            day , first, second, third, fourth = row
            print ("{:<8} {:<28} {:<28} {:<28} {:<28}".format(day,first,second,third,fourth))
        timetable_data_f.close()
    except:
        print("Generate the timetable first!\n")


def teacher_generate(teacher_id):
    # Display the time table of a particular teacher.
    try:
        timetable_data_f=open('timetable.json', "r+")
        timetable_data= json.loads(timetable_data_f.read())
        timetable=timetable_data['data']
        
        temp_Val=["Section 1","Section 2","Section 3","Section 4","Section 5"]
        days=['MON','TUE','WED','THU','FRI']
        first_period="08:00AM-09:00AM"
        second_period="09:00AM-10:00AM"
        third_period="11:00AM-12:00PM"
        fourth_period="12:00PM-01:00PM"

        periods=[first_period,second_period,third_period,fourth_period]


        print_var=[]
    
        for i in range(5):
            for row in (timetable[i][temp_Val[i]]):
                temp_var=[list(row.keys())[0]]
                for each_element in (list(row.values())[0]):
                    for iter_val in periods:
                        if(int(list(each_element[iter_val][1].keys())[0])==teacher_id):
                            temp_var.append(list(each_element[iter_val][0].values())[0])
                        else:
                            temp_var.append("NULL")
                print_var.append(temp_var)
    
        print_var2=[["Day",first_period,second_period,third_period,fourth_period]]

        for i in range(5):
            iter_val=(i)
            section=1
            new_row=[print_var[iter_val][0],"NULL","NULL","NULL","NULL"]
            while iter_val<25:
                #print(print_var[iter_val])
                for row in range(1,5):
                    if(new_row[row]=="NULL" and print_var[iter_val][row]!="NULL"):
                        ans=""+str(print_var[iter_val][row])+"(Section-"+str(section)+")"
                        new_row[row]=ans
                iter_val+=5
                section+=1
                if(section>5):
                    section=1

            print_var2.append(new_row)

 
        print("\nTIME TABLE OF Teacher ",str(teacher_id))
        print()
        for row in print_var2:
            day , first, second, third, fourth = row
            print ("{:<8} {:<28} {:<28} {:<28} {:<28}".format(day,first,second,third,fourth))
        timetable_data_f.close()
    except:
        print("Generate the timetable first!\n")

generate()