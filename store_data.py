"""
Module to store the information into json file.
Teachers data is stored in teacher.json
Subject data is stored in subject.json
"""

import json


def store_subject(subject_name):

    #store subject data in subject.json
    subject_data_f=open('subject.json', "r+")
    subject_data= json.loads(subject_data_f.read())
    sub_count=len(subject_data['data'])
    if(sub_count>4):
        print("Reached Maximum Subject Count\n")
    else:
        if(len(subject_data['data'])==0):
            if(subject_name==''):
                subject_data['data']=[{1:"sub1"}]
            else:
                subject_data['data']=[{1:subject_name}]
            subject_data_f.seek(0)
            json.dump(subject_data,subject_data_f)
            subject_data_f.close()
            print("Added ..!\n")

        else:
            top_value=int(list(subject_data['data'][-1].keys())[0])+1
            if(subject_name==''):
                subject_data['data'].append({str(top_value):"sub"+str(top_value)})
            else:
                subject_data['data'].append({str(top_value):subject_name})
        
            subject_data_f.seek(0)
            json.dump(subject_data,subject_data_f)
            subject_data_f.close()
            print("Added ..!\n")      
    

def store_teacher(teacher_name):
    #store teacher data in teacher.json
    teacher_data_f=open('teacher.json', "r+")  
    teacher_data= json.loads(teacher_data_f.read())
    teach_count=len(teacher_data['data'])
    if(teach_count>10):
        print("Reached Maximum Teacher Count\n")
    else:
        if(len(teacher_data['data'])==0):
            if(teacher_name==''):
                teacher_data['data']=[{1:"Teacher1"}]
            else:
                teacher_data['data']=[{1:teacher_name}]
            teacher_data_f.seek(0)
            json.dump(teacher_data,teacher_data_f)
            teacher_data_f.close()
            print("\nTecher ID is ==> 1")
        else:
            top_value=int(list(teacher_data['data'][-1].keys())[0])+1
            if(teacher_name==''):
                teacher_data['data'].append({str(top_value):"teacher"+str(top_value)})
            else:
                teacher_data['data'].append({str(top_value):teacher_name})
            teacher_data_f.seek(0)
            json.dump(teacher_data,teacher_data_f)
            teacher_data_f.close()
            print("\nTecher ID is ==> ",str(top_value))

