"""
This is the main module (User Interface)
"""

import json
import store_data
import generate_timetable

subject_data_f=open('subject.json', "r")
teacher_data_f=open('teacher.json', "r")
subject_data= json.loads(subject_data_f.read())
teacher_data= json.loads(teacher_data_f.read())

def add_subject_data(subject_name):
    # add subject data calling store_data module
    sub_count=len(subject_data['data'])
    if(sub_count>4):
        print("Reached Maximum Subject Count\n")
    else:
        store_data.store_subject(subject_name)

def add_teacher_data(teacher_name):
    # add teacher data calling store_data module
    teach_count=len(teacher_data['data'])
    if(teach_count>10):
        print("Reached Maximum Teacher Count\n")
    else:
        store_data.store_teacher(teacher_name)

def display_teacherwise(teacher_id):
    # display teacher time table using generate_timetable module
    teach_count=len(teacher_data['data'])
    if(teacher_id<=0 or teacher_id>teach_count):
        print("Invalid ID")
    else:
        generate_timetable.teacher_generate(teacher_id)

def display_sectionwise(section_id):
    # display section time table using generate_timetable module6
    if(section_id<=0 or section_id>5):
        print("Invalid Section Number")
    else:
        generate_timetable.section_generate(section_id)

def check_warning_condition():
    # checks warning condition with respect to subject and teacher constrains.
    subject_data_f=open('subject.json', "r")
    teacher_data_f=open('teacher.json', "r")
    subject_data= json.loads(subject_data_f.read())
    teacher_data= json.loads(teacher_data_f.read())
    
    sub_count=len(subject_data['data'])
    teacher_count=len(teacher_data['data'])
    if(sub_count<4 or teacher_count<11):
        return 1
    else:
        return 0
